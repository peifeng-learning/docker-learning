#!/usr/bin/env bash

set -eo pipefail

command=$1
param1=$2

k8s_version=v1.15.1
host_ip=$(ip a show eth0 | grep inet | awk '{print $2}' | awk -F"/" '{print $1}')

echo k8s_version=${k8s_version}, host_ip=${host_ip}

## 关闭 swap 分区
set_swap() {
swapoff -a
sed -i 's/.*swap.*/#&/' /etc/fstab || true
}

## 关闭防火墙
set_firewall() {
systemctl stop firewalld \
&& systemctl disable firewalld
}

## 关闭 seLinux
function set_seLinux() {
setenforce 0 || true
sed -i -re '/^\s*SELINUX=/s/^/#/' -e '$i\\SELINUX=disabled'  /etc/selinux/config || true
}

## ipv6 设置
function set_ipv6() {
cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
vm.swappiness=0
EOF
sysctl --system
}

## install docker
function install_docker() {
yum remove -y docker docker-common docker-selinux docker-engine \
&& yum install -y yum-utils device-mapper-persistent-data lvm2 \
&& yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo \
&& yum makecache fast \
&& yum install -y --setopt=obsoletes=0 docker-ce-18.09.7-3.el7

cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF

systemctl start docker
systemctl enable docker
}

## install kubeadm kubelet kubectl
function install_kube() {
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=http://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=http://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg http://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

yum makecache fast \
&& yum install -y kubelet kubeadm kubectl \
&& systemctl enable kubelet \
&& systemctl start kubelet
}

## 预拉取镜像， 从阿里云拉取
function pre_pull_images() {
ALI=registry.cn-hangzhou.aliyuncs.com/google_containers/
K8S=k8s.gcr.io/
echo repo = ${ALI}
echo tag = ${K8S}
images=("kube-apiserver:v1.15.1" "kube-controller-manager:v1.15.1" "kube-scheduler:v1.15.1" "kube-proxy:v1.15.1" "etcd:3.3.10" "pause:3.1" "coredns:1.3.1")
for image in ${images[@]} ; do
    docker pull ${ALI}${image} && docker tag ${ALI}${image} ${K8S}${image}
    docker rmi ${ALI}${image}
done
}

function common_setup() {
    set_swap
    set_firewall
    set_seLinux
    set_ipv6
    install_docker
    install_kube
    pre_pull_images
}

function k8s_install() {
    if [[ -z ${param1} ]]; then
       param1=10.244.0.0
    fi
    kubeadm init --kubernetes-version=${k8s_version} --pod-network-cidr=${param1}/16 --apiserver-advertise-address=${host_ip}

    mkdir -p $HOME/.kube
    cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    chown $(id -u):$(id -g) $HOME/.kube/config
}

function k8s_reset() {
    kubeadm reset
    rm -fr $HOME/.kube
}

function flannel_install() {
    kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
}

function zsh_install() {
    yum -y update && yum -y install zsh git \
    && sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}

function hostname_setup() {
    hostnamectl set-hostname
    echo -e "\n\n172.16.21.73   master\n172.16.21.74   node1\n172.16.21.72   node2" >> /etc/hosts
}


function main() {
   usage="Usage: $0 (common|hostname|zsh|k8s|flannel|k8s-reset) parameter"
   echo ${usage}

   case ${command} in
    common)
    common_setup
    ;;
    k8s)
    k8s_install
    ;;
    hostname)
    hostname_setup
    ;;
    zsh)
    zsh_install
    ;;
    flannel)
    flannel_install
    ;;
    k8s-reset)
    k8s_reset
    ;;
    *)
    echo command not support yet. please check it again
    exit 0
    ;;
   esac
}

main
