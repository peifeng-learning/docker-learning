#!/usr/bin/env bash

# docker run -p 8500:8500 -p 53:53/udp -h node1 consul:1.5.2 -server -bootstrap

docker run -d -p 8500:8500 -v /data/consul:/consul/data -e CONSUL_BIND_INTERFACE='eth0' --name=consul_server_1 consul:1.5.2 agent -server -bootstrap -ui -node=1 -client='0.0.0.0'
