#!/usr/bin/env bash

set -exo pipefail

command=$1
param1=$2

function git_install() {
    yum -y update \
     && yum -y install git
}

function zsh_install() {
    git_install
    yum -y install zsh
}

function oh_my_zsh_install() {
    zsh_install
   sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}

# install powerline if necessary
function powerline_install() {
   git clone https://github.com/powerline/fonts.git --depth=1 \
   && cd fonts \
   && ./install.sh \
   && cd .. \
   && rm -fr ./fonts
}

# add user
function add_user() {
    if [[ -z ${param1} ]]; then
       param1="dylan"
    fi
    adduser ${param1}
    passwd ${param1}
}

## generate ssh
function generate_ssh() {
    ssh-keygen -t rsa -C "peifeng"
    touch $HOME/.ssh/config
}


function docker_install() {
yum remove -y docker docker-common docker-selinux docker-engine \
&& yum install -y yum-utils device-mapper-persistent-data lvm2 \
&& yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo \
&& yum makecache fast \
&& yum -y install docker-ce

if [[ -z ${param1} ]]; then
    param1="https://docker.mirrors.ustc.edu.cn"
fi

cat > /etc/docker/daemon.json <<EOF
{
  "registry-mirrors": ["${param1}"]
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF

systemctl start docker
systemctl enable docker
}

function add_user_to_docker_group() {
    gpasswd -a ${USER} docker
    newgrp docker
}

function java_install() {
   yum install -y java-1.8.0-openjdk-devel.x86_64
}

function maven_install() {
    wget http://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.tar.gz
    tar -zxvf apache-maven-3.6.1-bin.tar.gz
    sudo mv ./apache-maven-3.6.1 /usr/share/
    echo "export PATH=$PATH:/usr/share/apache-maven-3.6.1/bin" | sudo tee -a /etc/profile
    source /etc/profile
    mvn --version
}



function main() {
    usage="Usage: $0 (all|zsh|docker|java|maven)"
    echo ${usage}

    case ${command} in
    all)
         oh_my_zsh_install
         powerline_install
         generate_ssh
         add_user
         docker_install
         add_user_to_docker_group
         java_install
    ;;
    zsh)
        oh_my_zsh_install
    ;;
    docker)
        docker_install
    ;;
    java)
        java_install
    ;;
    maven)
        maven_install
     ;;
    *)
        echo "unsupported command yet"
        exit 0
    ;;
    esac
}

main
